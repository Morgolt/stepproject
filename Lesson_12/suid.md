#complile and set permitions
$ gcc program.c -o program
$ sudo chown root:root program
$ sudo chmod 4755 program
$ ./program


#program.c
   #include <stdio.h>
   #include <stdlib.h>
   #include <sys/types.h>
   #include <unistd.h>

   int main()
   {
     setuid(0);
     system("./program.sh");
     return 0;
   }

#program.sh
#!/bin/bash
echo "User ID: $UID"
echo "Effective User ID: $EUID"
