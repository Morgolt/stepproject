- `crontab -e`: Редактирует крон задания для текущего пользователя.
- `crontab -l`: Показывает крон задания для текущего пользователя.
- Формат записи:
Каждое задание в cron таблице имеет следующий формат:
```api
* * * * * command
```
- **Минуты (0-59):** Это цифра, представляющая минуту часа. Например, значение `5` означает "в 5 минут каждого часа".
- **Часы (0-23):** Это число, представляющее час дня в 24-часовом формате. Например, значение `12` означает "в 12 часов дня".
- **Дни месяца (1-31):** Это число, представляющее день месяца. Например, значение `15` означает "15-ое число каждого месяца".
- **Месяцы (1-12):** Это число, представляющее месяц года. Например, значение `7` означает "в июле"
- **Дни недели (0-7):** Это число или буква, представляющие день недели. Значения `0` и `7` обычно интерпретируются как воскресенье. Буквы также допустимы: `0` или `7` для воскресенья, `1` для понедельника и т. д. Например, значение `5` означает "в пятницу".

