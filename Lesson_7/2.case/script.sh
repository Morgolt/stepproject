#!/bin/bash

while true; do
    echo "Введите вашу операционную систему (linux, mac или windows) или 'exit' для выхода:"
    read input

    os=$(echo "$input" | tr '[:upper:]' '[:lower:]')

    case "$os" in
        exit)
            echo "Выход из программы."
            break
            ;;
        linux)
            echo "Привет хакер!"
            ;;
        mac|macos)
            echo "Привет маковод!"
            ;;
        windows)
            echo "Здесь нет никого."
            ;;
        *)
            echo "Неверный ввод. Пожалуйста, введите linux, mac, windows или 'exit'."
            ;;
    esac
done

