#!/bin/bash

string="Hello, World!"

# Извлечение подстроки начиная с 7-го символа длиной 5 символов
substring=${string:0:5}

echo "Извлеченная подстрока: \"$substring\""

