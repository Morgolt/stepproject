#!/bin/bash

echo "Введите ваш возраст:"
read age

if (( age < 18 )); then
    echo "Ты слишком юн."
elif (( age == 18 )); then
    echo "Подожди чуть-чуть."
else
    echo "Заберите свой пакет."
fi
