  wget https://github.com/htop-dev/htop/archive/refs/tags/3.3.0.tar.gz
  tar -xvf 3.3.0.tar.gz 
  cd htop-3.3.0/
  vim README.md
  sudo apt-get install autotools-dev
  sudo apt-get install autoconf
  sudo apt install libncursesw5-dev
  ./autogen.sh && ./configure && make
  ./configure MAKE="gmake"
  ./configure --disable-dependency-tracking
  sudo apt install make
  sudo make install
  htop
