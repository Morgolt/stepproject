### Изменение имени хоста

1. **Получение текущего имени хоста:** Используйте команду `hostname`, чтобы узнать текущее имя хоста.
    ```bash
    hostname
    ```

2. **Изменение имени хоста:** Выполните команду `sudo hostname newhost`, чтобы изменить имя хоста на "newhost".
    ```bash
    sudo hostname newhost
    ```

3. **Редактирование файла имени хоста:** Откройте файл `/etc/hostname` в редакторе Vim с помощью команды `sudo vim /etc/hostname`. Повторите с `/etc/hosts`
    ```bash
    sudo vim /etc/hostname
    ```

4. **Проверка файлов юнитов для сетевых сервисов:** Используйте команду `systemctl list-unit-files | grep -i network`, чтобы вывести список файлов юнитов и отфильтровать только те, которые относятся к сети.
    ```bash
    systemctl list-unit-files | grep -i network
    ```

5. **Перезапуск сервиса сетевой подсистемы:** Перезапустите сервис например systemd-networkd с помощью команды `sudo systemctl restart systemd-networkd.service`.
    ```bash
    sudo systemctl restart systemd-networkd.service
    ```

6. **Выход из текущей сессии:** Выйдите из текущей сессии с помощью команды `logout`.
    ```bash
    logout
    ```

### Подключение к виртуальной машине через SSH

1. **Установка пакета openssh-server:** Установите пакет openssh-server, выполнив команду `sudo apt install openssh-server`.
    ```bash
    sudo apt install openssh-server
    ```

2. **Установка редактора Vim:** Установите редактор Vim, используя команду `sudo apt install vim`.
    ```bash
    sudo apt install vim
    ```

3. **Редактирование файла конфигурации SSH:** Откройте файл конфигурации SSH `/etc/ssh/sshd_config` в редакторе Vim.
    ```bash
    sudo vim /etc/ssh/sshd_config
    ```

4. **Установка параметра PermitRootLogin:** В файле `sshd_config` установите параметр `PermitRootLogin yes`, разрешая вход под пользователем root.
    ```bash
    PermitRootLogin yes
    ```

5. **Перезапуск сервиса SSH:** Перезапустите сервис SSH, введя команду `systemctl restart ssh`.
    ```bash
    sudo systemctl restart ssh
    ```

6. **Установка пакета net-tools:** Установите пакет net-tools, используя команду `apt install net-tools`.
    ```bash
    sudo apt install net-tools
    ```

7. **Просмотр информации о сетевых интерфейсах:** Выведите информацию о сетевых интерфейсах с помощью команды `ifconfig`.
    ```bash
    ifconfig
    ```

8. **SSH-подключение к виртуальной машине:** Установите SSH-соединение с виртуальной машиной, введя команду `ssh user@192.168.1.10`, где "user" - имя пользователя, а "192.168.1.10" - IP-адрес виртуальной машины.
    ```bash
    ssh user@192.168.1.10
    ```
